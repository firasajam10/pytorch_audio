# Requirements

Die in diesem Projekt verwendeten Python Paketbibliotheken können  [hier](https://gitlab.com/firasajam10/pytorch_audio/-/blob/main/requirements.txt) eingesehen werden.

# UrbanSound8K Datenset
 
Die Rohdaten des Projektes sowie Hinweise zum Training mit dem Datenset können [hier](https://urbansounddataset.weebly.com/urbansound8k.html) entnommen werden. 

Die vorverarbeiteten gesamten Audiodaten sind im Ordner [Datenset](https://gitlab.com/firasajam10/pytorch_audio/-/tree/main/Datenset) mit einer Abtastrate von 44,1 kHz und 22,05 kHz in nicht quantisierter und quantisierter Form (Wertebereich -128 bis 127) bereitgestellt. 
Zusätzlich sind die Daten mit den beiden Abtastraten foldweise quantisiert und nicht quantisiert verfügbar.
Für andere Konfigurationen können die Rohdaten [hier](https://urbansounddataset.weebly.com/download-urbansound8k.html) heruntergeladen und mit [urbansounddataset.py](https://gitlab.com/firasajam10/pytorch_audio/-/wikis/Urbansound8Kdatenset) Veränderungen vorgenommen werden.

# Projektbeschreibung

Mit diesem Projekt kann ein CNN in quantisierter und nicht quantisierter Form zur Audiodatenklassifikation trainiert werden. Zudem sind die Eingabeaudiofiles in quantisierter Form im Ordner "Datenset" mit 44.1 kHz und 22.05 kHz Abtastrate abgelegt.

Die Verarbeitungspipeline ist in drei Schritte gegliedert:

1. Vorverarbeitung und Abspeichern der Daten mit [urbansounddataset.py](https://gitlab.com/firasajam10/pytorch_audio/-/blob/main/urbansounddataset.py) (Anleitung: [UrbanSound8K Datenset](https://gitlab.com/firasajam10/pytorch_audio/-/wikis/UrbanSound8K-Datenset))
2. Training und Abspeichern des trainierten CNN mit [training.py](https://gitlab.com/firasajam10/pytorch_audio/-/blob/main/training.py) (Anleitung: [Training](https://gitlab.com/firasajam10/pytorch_audio/-/wikis/Training))
3. Extraktion der Parameter des CNN mit [inference.py](https://gitlab.com/firasajam10/pytorch_audio/-/blob/main/inference.py) (Anleitung: [Inference](https://gitlab.com/firasajam10/pytorch_audio/-/wikis/Inference))
