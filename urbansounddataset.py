import os
import matplotlib.pyplot as plt
import numpy as np
import torch
from torch.utils.data import Dataset
import pandas as pd
import torchaudio


class UrbanSoundDataset(Dataset):
    """
    annotations_file = Pfad zu der CSV-Datei
           audio_dir = Pfad zu den Audiodateien
      transformation = Transformation in Melspektrogram
  target_sample_rate = Zielabtastrate
         num_samples = Laenge des Signals
              device = Cpu oder gpu
 quantisierteAusgabe = False oder true
              nrFold = Nur ein Fold angeben zum Abspeichern
    """
    def __init__(self,
                 annotations_file,
                 audio_dir,
                 transformation,
                 target_sample_rate,
                 num_samples,
                 device,
                 quantisierteAusgabe = False,
                 nrFold = None):
        self.annotations = pd.read_csv(annotations_file)
        self.audio_dir = audio_dir
        self.device = device
        self.transformation = transformation.to(self.device)
        self.target_sample_rate = target_sample_rate
        self.num_samples = num_samples
        self.quantisiert = quantisierteAusgabe
        if nrFold is not None:
           self.selectedFold = "fold" + str(nrFold)
        else:
           self.selectedFold = None

    # Auslesen der Laenge der CSV Datei
    def __len__(self):
        return len(self.annotations)

    """
    Sobald auf ein Mel-Spektrogramm Zugegriffen wird,
    1. Label der Audiodatei und Pfad sowie der Fold ausgelesen
    2. das Signal geladen sowie die sample Rate
    3. das Signal auf eine einheitliche Samplerate gesampled (wenn nötig)
    4. aus einem Stereosignal ein Monosignal gemacht (wenn nötig)
    5. die Länge des Signals auf die Anzahl der Samples festgelegt
    6. falls das Signal nicht lang genug ist werden die restlichen Samples mit 0 aufgefüllt
    7. Das Audiosignal in ein Mel-Spektrogramm umwandeln
    """
    def __getitem__(self, index):
        audio_sample_path, fold = self._get_audio_sample_path(index)
        label = self._get_audio_sample_label(index)
        signal, sr = torchaudio.load(audio_sample_path)
        signal = signal.to(self.device)
        signal = self._resample_if_necessary(signal, sr)
        signal = self._mix_down_if_necessary(signal)
        signal = self._cut_if_necessary(signal)
        signal = self._right_pad_if_necessary(signal)
        #signal = self.transformation(signal)
        # Returned alle Daten
        if self.selectedFold == None:
            if self.quantisiert == True:
                signal = signal.to(dtype = torch.int8)
                signal = signal.to(dtype = torch.float32)
                return signal, label
            else:
                return signal, label
        # Returns nur den fold der gewaehlt wurde
        elif self.selectedFold == fold:
            if self.quantisiert == True:
                signal = signal.to(dtype=torch.int8)
                signal = signal.to(dtype=torch.float32)
                return signal, label
            else:
                return signal, label
        else:
            return None

    # Das Audiosignal kuerzer machen,
    def _cut_if_necessary(self, signal):
        if signal.shape[1] > self.num_samples:
            signal = signal[:, :self.num_samples]
        return signal

    # Signal verlaengern, wenn es nicht lang genug ist mit 0
    def _right_pad_if_necessary(self, signal):
        length_signal = signal.shape[1]
        if length_signal < self.num_samples:
            num_missing_samples = self.num_samples - length_signal
            last_dim_padding = (0, num_missing_samples)
            signal = torch.nn.functional.pad(signal, last_dim_padding)
        return signal

    # Neu Samplen wenn das Signal nicht passend aufgenommen wurde
    def _resample_if_necessary(self, signal, sr):
        if sr != self.target_sample_rate:
            resampler = torchaudio.transforms.Resample(sr, self.target_sample_rate)
            signal = resampler(signal)
        return signal

    # aus einem Stereosignal ein Monosignal durch den Mittelwert machen
    def _mix_down_if_necessary(self, signal):
        if signal.shape[0] > 1:
            signal = torch.mean(signal, dim=0, keepdim=True)
        return signal

    # Extrahieren des Audiopfades durch das vorgegeben Labeling in Spalte 5
    def _get_audio_sample_path(self, index):
        fold = f"fold{self.annotations.iloc[index, 5]}"
        path = os.path.join(self.audio_dir, fold, self.annotations.iloc[
            index, 0])
        return path, fold

    # Auslesen des Labels aus der Klasse
    def _get_audio_sample_label(self, index):
        return self.annotations.iloc[index, 6]

    # Auslesen der Dateibezeichnung der Audiodatei
    def _get_audio_sample_Text(self, index):
        return self.annotations.iloc[index, 0]

if __name__ == "__main__":
    ANNOTATIONS_FILE = r"C:\Users\T.B\Desktop\UrbanSound8K\metadata\UrbanSound8K.csv"
    AUDIO_DIR = r"C:\Users\T.B\Desktop\UrbanSound8K\audio"
    SAMPLE_RATE = 44100#22050
    NUM_SAMPLES = 44100#22050

    if torch.cuda.is_available():
        device = "cuda"
    else:
        device = "cpu"
    print(f"Using device {device}")
    device = "cpu"
    mel_spectrogram = torchaudio.transforms.MelSpectrogram(
        sample_rate=SAMPLE_RATE,
        n_fft=1024,
        hop_length=512,
        n_mels=64
    )
    quantisiert = True # Daten auf int8 quantisieren oder nicht
    saveFolds = False # Daten in der Folds speichern oder alle Daten
    liste = []
    nrFoldsAudio = np.zeros(11)
    if not saveFolds:
        # speichert alle Daten sofort ab
        usd = UrbanSoundDataset(ANNOTATIONS_FILE,
                                AUDIO_DIR,
                                mel_spectrogram,
                                SAMPLE_RATE,
                                NUM_SAMPLES,
                                device,
                                quantisiert)
        print(f"There are {len(usd)} samples in the dataset.")
        for i in range(0, len(usd)):
            elem = usd[i]
            if elem == None:
                continue
            liste.append(elem)
        if quantisiert:
          torch.save(liste, "datasetQuantisiert.pt")
        else:
          torch.save(liste, "dataset.pt")
        print(f"There are {len(liste)} Audiofiles saved.")
    else:
     # Speichert jedes Fold einzeln ab
     for foldNr in range(1, 11):
        usd = UrbanSoundDataset(ANNOTATIONS_FILE,
                            AUDIO_DIR,
                            mel_spectrogram,
                            SAMPLE_RATE,
                            NUM_SAMPLES,
                            device,
                            quantisiert,
                            foldNr)
        print(f"There are {len(usd)} Audiofiles in the dataset.")
        count = 0
        for i in range(0, len(usd)):
            elem = usd[i]
            if elem == None:
                continue
            count = count + 1
            liste.append(elem)
        print(len(liste))
        print(count)
        nrFoldsAudio[foldNr] = count
        torch.save(liste, "fold" + str(foldNr) + ".pt")
        print("finished " +"fold" + str(foldNr) + ".pt")
        print("--------------------------------------")
     x = np.arange(10)
     plt.bar(x, height=[nrFoldsAudio[1], nrFoldsAudio[2], nrFoldsAudio[3], nrFoldsAudio[4],
                       nrFoldsAudio[5], nrFoldsAudio[6], nrFoldsAudio[7], nrFoldsAudio[8], nrFoldsAudio[9], nrFoldsAudio[10]])
     plt.xticks(x, ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'], fontsize=12)
     plt.ylabel('Anzahl Audiofiles', fontsize=12)
     plt.xlabel('Fold', fontsize=12)
     plt.title(f'Anzahl Audiofiles je Fold', fontsize=12)
     plt.show()
