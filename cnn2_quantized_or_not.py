from torch import nn
import torch
import matplotlib.pyplot as plt
import torch.nn.functional as F

class convNet(nn.Module):
    """
    booleanQuantisiert: if true wird das Netz zu jedem Schritt im Int8 Wertebereich trainiert
    booleanBias: if true hat jeder conv Layer einen bias sonst nicht
    """
    def __init__(self, booleanQuantisiert, booleanBias):
        self.boolQuantisiert = booleanQuantisiert
        self.boolBias = booleanBias
        super().__init__()
        # QuantStub converts tensors from floating point to quantized
        if booleanQuantisiert:
            self.quant = torch.quantization.QuantStub()
        # 4 conv blocks / flatten / linear / softmax

        # Layer 1,
        self.conv1 = nn.Sequential(
            nn.Conv2d(in_channels=1, out_channels=8, kernel_size=(3, 3)),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=(2, 2)))

        # Layer 2,
        self.conv2 = nn.Sequential(
            nn.Conv2d(in_channels=8, out_channels=16, kernel_size=(3, 3)),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=(2, 2)))

        # Layer 3,4
        self.conv3 = nn.Sequential(
            nn.Conv2d(in_channels=16, out_channels=32, kernel_size=(3, 3)),
            nn.ReLU(),
            nn.Conv2d(in_channels=32, out_channels=64, kernel_size=(3, 3)),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=(2, 2)))

        # Fully Connected layer 1
        self.fc1 = nn.Linear(in_features=64 * 4 * 10, out_features=512)

        # Fully Connected layer 2,
        self.fc2 = nn.Linear(in_features=512, out_features=256)

        # Fully Connected layer 3,
        self.fc3 = nn.Linear(in_features=256, out_features=128)

        # Fully Connected layer 4,
        self.fc4 = nn.Linear(in_features=128, out_features=10)

        self.flatten = nn.Flatten()

        if booleanQuantisiert:
            self.dequant = torch.quantization.DeQuantStub()

    """ 
    Forwardpath des CNN
    self.boolQuantisiert: if true quantisierten Forwardpath nutzen
    """
    def forward(self, input_data):
        if self.boolQuantisiert:
            x = self.quant(input_data)
            self.checkBounds(input_data)
            x = self.conv1(x)
            x = self.valueInt8Mapping(x)
            self.checkBounds(x)
            x = self.conv2(x)
            x = self.valueInt8Mapping(x)
            self.checkBounds(x)
            x = self.conv3(x)
            x = self.valueInt8Mapping(x)
            self.checkBounds(x)

            x = self.flatten(x)


            x = F.relu(self.fc1(x))
            x = F.relu(self.fc2(x))
            x = F.relu(self.fc3(x))
            x = self.fc4(x)

            x = self.dequant(x)
            return x
        else:
            x = self.conv1(input_data)
            x = self.conv2(x)
            x = self.conv3(x)

            x = x.view(-1, self.num_flat_features(x))

            x = F.relu(self.fc1(x))
            x = F.relu(self.fc2(x))
            x = F.relu(self.fc3(x))
            x = self.fc4(x)
            x = self.flatten(x)
            return x

    def num_flat_features(self, x):
        size = x.size()[1:]
        n_features = 1
        for s in size:
            n_features = n_features * s

        return n_features

    # zum kalibrieren des quantisierten Modells nach dem Training
    def evalQuant(self, input_data):
        if self.boolQuantisiert:
            x = self.quant(input_data)
            x = torch.clamp(x, min=-128, max=127)
            x = self.conv1(x)
            x = torch.clamp(x, min=-128, max=127)
            x = self.conv2(x)
            x = torch.clamp(x, min=-128, max=127)
            x = self.conv3(x)
            x = torch.clamp(x, min=-128, max=127)

            x = self.flatten(x)

            x = F.relu(self.fc1(x))
            x = torch.clamp(x, min=-128, max=127)
            x = F.relu(self.fc2(x))
            x = torch.clamp(x, min=-128, max=127)
            x = F.relu(self.fc3(x))
            x = torch.clamp(x, min=-128, max=127)
            x = self.fc4(x)
            x = torch.clamp(x, min=-128, max=127)
            x = self.dequant(x)
            return x

    """
    Formt alle Werte des Tensors x in den Integerbereich und rundet die Zahlen
    auf ganze Zahlen nach jedem Layer
    """
    def valueInt8Mapping(self, x):
        x = torch.clamp(x, min=-128, max=127)
        x = x.to(dtype=torch.int8)
        x = x.to(dtype=torch.float32)
        return x

    # Darstellen der Werte von x in einem Histogramm
    def darstellenHistogram(self, x):
        help = x.view(-1)
        print("max: " + str(help.max()))
        print("min: " + str(help.min()))
        """k = torch.tensor([-1.45, -1.57, -3.5, 3.8, 7.2], dtype=torch.float32)
        k = k.to(dtype=torch.int8)
        k = k.to(dtype=torch.float32)
        print(k)"""
        help = help.to("cpu")
        help = help.detach().numpy()
        groesser127 = help > 127
        plt.hist(help, bins=100)
        plt.show()

    # Check ob ein Wert im Tensor groesser oder kleiner des Int8 Bereiches ist
    def checkBounds(self, x):
        if x.max() > 127:
            print("bigger: " + str(x.max()) )
        elif x.min() < -128:
            print("less: " + str(x.min()))