import torch
import os
# import random
import matplotlib.pyplot as plt
import numpy as np
from torch import nn
from torch.utils.data import DataLoader
from cnn2_quantized_or_not import convNet

BATCH_SIZE = 32
EPOCHS = 10
LEARNING_RATE = 0.001

"""
@ return die praedizierte Klasse aufgrund von input
quantisiert: bei true wird die Praediktion des quantisierten Modells verwendet
normiert: bei true ist wird der Input vorher 
mit input = (input - mean(input)) / std(input) normiert
"""
def predict(model, input, quantisiert, normiert=False):
    model.eval()
    with torch.no_grad():
        if normiert:
            input_m, input_s = input.mean(), input.std()
            input = (input - input_m) / input_s
        if quantisiert:
            predictions = model.evalQuant(input)
        else:
            predictions = model(input)
        # Tensor (1, 10) -> [ [0.1, 0.01, ..., 0.6] ]
        predicted_index = predictions[0, :].argmax(0)
        predicted = predicted_index
    return predicted


"""
Zaehlt die Anzahl der richtigen und flasch praedizierten Audiodateien und liefert sie zurueck
model: CNN
usd: UrbanDataset 
anz: Anzahl der zu testenden Audiodateien
quantisiert: bei true wird die Praediktion des quantisierten Modells verwendet
normiert: bei true ist wird der Input vorher 
mit input = (input - mean(input)) / std(input) normiert 
"""
def testeWerte(model, usd, anz, device1, quantisiert, normiert=False):
    anzRichtig = 0
    anzFalsch = 0
    model = model.to(device1)
    for i in range(0, anz):
        input, target = usd[i][0], usd[i][1]  # [batch size, num_channels, fr, time]
        input = input.to(device1)
        if input.dim() == 3:
            input.unsqueeze_(0)  # fuegt die 4te Dimension hinzu
        predicted = predict(model, input, quantisiert, normiert)
        if predicted == target:
            anzRichtig = anzRichtig + 1
        else:
            anzFalsch = anzFalsch + 1

    return anzRichtig, anzFalsch


"""
Berechnet wie viele Testsample richtig und falsch nach dem Training praediziert wurden und plottet das Ergebnis 
mit der Genauigkeit des Models
model: CNN
usd: UrbanDatasound extrahierte Melspektrogrammliste
anzTestsample: Anzahl Testaudios
quantisiert: bei true wird die Praediktion des quantisierten Modells verwendet
normiert: bei true ist wird der Input vorher 
mit input = (input - mean(input)) / std(input) normiert
"""
def darstellenTrueFalse(model, usd, anzTestsample, device1, quantisiert, normiert=False):
    model.eval()
    richtig, falsch = testeWerte(model, usd, anzTestsample, device1, quantisiert, normiert)
    accurancy = 100 * richtig / (richtig + falsch)
    x = np.arange(2)
    plt.figure()
    plt.bar(x, height=[richtig, falsch])
    plt.xticks(x, ['true', 'false'], fontsize=12)
    plt.ylabel('Anzahl Audiofiles', fontsize=12)
    plt.title(f'Spektrogramme: {anzTestsample}\n Accurancy: {accurancy:.2f} %, Epochen: {EPOCHS}', fontsize=12)


"""
Quantisiert die Gewichte des Models 
usd: Urbandataset 
epochs: Anzahl wie oft das Datenset testweise praediziert wird fuer die Postquantisierung
quantisiert: Wichtig zu setzen für predict des Neuronalen Netzes (siehe cnnNet)
epochs: Anzahl wie oft alle Trainingsdaten durch das Netzt gehen sollen zur kalibrierung waerend der Kalibrierung
"""
def quantisiertesNetzPostQuant(model, usd, device, quantisiert = True,  epochs=1):
    cnn = model.to(device)
    cnn.eval()
    cnn.qconfig = torch.quantization.get_default_qconfig('fbgemm')
    cnn = torch.quantization.prepare(cnn, inplace=True)
    # Prepare model for quantisation
    for i in range(0, epochs):
        calibrate(cnn, usd, device, quantisiert)
        print("Epoche kalibriere: " + str(i))
        print("-----------------")
    cnn = model.to("cpu")
    cnn = torch.quantization.convert(cnn, inplace=True)
    return cnn


"""
Kalibriere das Modell fuer die Postquantisierung
usd: UrbanDataSound Datenset
device: cpu/ cuda
quantisiert: Wichtig zu setzen für predict des Neuronalen Netzes (siehe cnnNet)
"""
def calibrate(model, usd, device, quantisiert = True):
    model.eval()
    model.to(device)
    i = 0
    with torch.no_grad():
        for input, target in usd:
            input, target = input.to(device), target
            if input.dim() == 3:
                input.unsqueeze_(0)
            # prediction = model(input)
            prediction = predict(model, input, quantisiert)  # Praediktion mit kalibrierten Modell


# Ausgabe der Layer, Datentypen der Gewichte und der Gewichte
def netzPrint(model):
    for name, weight in model.state_dict().items():
        try:
            print([name, weight.dtype, weight])
        except:
            continue


"""
Trainiert das CNN so oft wie folds vorhanden sind

dataFoldName: Ordnername wo die Folds abgespeichert sind 
model: CNN
loss_fn: Verlustfunktion 
optimiser: Optimierer 
device: Cpu/ cuda
epochs: Anzahl der Epochen
"""
def trainFolds(dataFoldName, model, loss_fn, optimiser, device, epochs):
    # 10 mal trainieren und 10 mal evaluieren
    path = []
    for i in range(1, 11):
        path.append(os.getcwd() + "\\Datenset\\" + dataFoldName + "\\fold" + str(i) + ".pt")
    # Erstellung der 10 folds
    folds = []
    # Laden der abgespeicherten Folds
    for name in path:
        folds.append(torch.load(name))
    avarage_lossGesamt = 0
    accurancyGesamt = 0
    accStore = []
    for i in range(0, len(path)):
        nr = 0
        usdTrain = []
        usdTest = []
        j = i
        print("------------------")
        for k in range(0, 10):
            j = j % 10
            if nr < 9:
                usdTrain += folds[j]
                nr = nr + 1
                print("Trainfold: " + str(j + 1))
            else:
                usdTest += folds[j]
                print("Testfold: " + str(j + 1))
            j = j + 1

        print(f'Start Training: {i + 1}')
        train(model, DataLoader(usdTrain, batch_size=BATCH_SIZE, shuffle=False), loss_fn, optimiser, device, epochs,
              evaluieren=False)
        avg_loss, acc = trainEvaluation(i, epochs, DataLoader(usdTest, batch_size=BATCH_SIZE, shuffle=False))
        accurancyGesamt += acc
        accStore.append(acc * 100)
        avarage_lossGesamt += avg_loss
        print(f'Fold Validierung Epoche: {i + 1}, Loss: {avg_loss:.2f}, Accuracy: {acc:.2f}')
    accurancyGesamt = accurancyGesamt / 10
    print("------")
    print(f'Gesamt Accuracy: {accurancyGesamt:.2f}')
    print("Foldtest Ende")
    foldAuswertung(accurancyGesamt, accStore)


"""
Plotten der Accurancy nach dem K-Fold Training 
  gesAccurancy: gemittelte Genauigkeit ueber alle k Trainings
  acc: Genauigkeit beim k-ten Training
"""
def foldAuswertung(gesAccurancy, acc):
    gesAccurancy = gesAccurancy * 100
    x = np.arange(10)
    plt.figure()
    plt.bar(x, height=[acc[1], acc[2], acc[3], acc[4],
                       acc[5], acc[6], acc[7], acc[8], acc[9], acc[0]])
    plt.xticks(x, ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'], fontsize=12)
    plt.yticks(np.arange(0, 101, 10))
    plt.axhline(y=gesAccurancy, xmin=min(x), xmax=max(x), c='#A50062')
    plt.ylabel('Accurancy Testfoldnummer in %', fontsize=12)
    plt.xlabel('Nummer Testfold', fontsize=12)
    plt.title(f'10 Testfolds \n Gesamt Accurancy: {gesAccurancy:.2f} %, Anz. Epochen: 10 x {EPOCHS}', fontsize=12)


"""
Trainiert das Model
model: CNN
data_loader: Datenset
loss_fn: Lossfunktion (Kreuzentropie etc.)
optimiser: Optimierer (Adam etc.)
device: cpu/ cuda
epochen: Anzahl
evaluieren: true wird die Accurancy fuer das fuer den batch auf der Konsole angezeigt
normiert: bei true ist wird der Input vorher 
mit input = (input - mean(input)) / std(input) normiert
"""
def train(model, data_loader, loss_fn, optimiser, device, epochs, evaluieren=True, normalise=False):
    scheduler = torch.optim.lr_scheduler.OneCycleLR(optimiser, max_lr=0.001,
                                                    steps_per_epoch=len(data_loader),
                                                    epochs=epochs,
                                                    anneal_strategy='linear')
    for i in range(epochs):
        model.train()
        for input, target in data_loader:
            input, target = input.to(device), target.to(device)
            if normalise:
                input_m, input_s = input.mean(), input.std()
                input = (input - input_m) / input_s
            # calculate loss
            prediction = model(input)
            loss = loss_fn(prediction, target)
            # backpropagate error and update weights
            optimiser.zero_grad()
            loss.backward()
            optimiser.step()
            scheduler.step()
        if evaluieren:
            avg_loss, acc = trainEvaluation(i, epochs, data_loader)
            print(f'Epoch: {i}, Loss avg: {avg_loss:.2f}, Accuracy: {acc:.2f}')
            print(f"Epoch: {i} loss: {loss.item()}")
            print("---------------------------")
    print("Finished training")


"""
Evaluiert nach einer Trainingsepoche
zu allen Daten in data_loader wird eine Praediktion vorgenommen und folgendes berechnet
- Running loss
- Accurancy 
"""
def trainEvaluation(i, epochs, data_loader):
    running_loss = 0.0
    correct_prediction = 0
    total_prediction = 0
    # Ausrechnen der Accurancy nur (kann ausgelassen werden bitte anpassen)
    # if i in range(0, epochs):  # == epochs-1:
    model.eval()
    with torch.no_grad():
        for input, target in data_loader:
            input, target = input.to(device), target.to(device)
            # berechne loss
            prediction = model(input)
            loss = loss_fn(prediction, target)
            running_loss += loss.item()
            # Prediction mit hoestem Score
            _, prediction = torch.max(prediction, 1)
            # Zaehle predictions die mit dem target label stimmen
            correct_prediction += (prediction == target).sum().item()
            total_prediction += prediction.shape[0]
        acc = correct_prediction / total_prediction
        # gebe die Anzahl der Batches aus
        num_batches = len(data_loader)
        avg_loss = running_loss / num_batches
        return avg_loss, acc


"""
 Sortiert nach klassen und bildet 1 Liste
 anzTrainingsdaten: Definiert wie viele Audiodaten im Trainingsset sind Rest Testdaten
 @returns 2 listen Trainings und Testdaten
"""
def splitDataSet(usd, anzTrainingsdaten):
    if (len(usd) >= anzTrainingsdaten):
        # erstelle Liste von Listen
        lst = []
        for j in range(10):
            lst.append([])
        for i in range(0, len(usd)):
            if usd[i][1] == 0:
                lst[0].append(usd[i])
            elif usd[i][1] == 1:
                lst[1].append(usd[i])
            elif usd[i][1] == 2:
                lst[2].append(usd[i])
            elif usd[i][1] == 3:
                lst[3].append(usd[i])
            elif usd[i][1] == 4:
                lst[4].append(usd[i])
            elif usd[i][1] == 5:
                lst[5].append(usd[i])
            elif usd[i][1] == 6:
                lst[6].append(usd[i])
            elif usd[i][1] == 7:
                lst[7].append(usd[i])
            elif usd[i][1] == 8:
                lst[8].append(usd[i])
            else:
                lst[9].append(usd[i])
        # fuege alle Elemente zu einer Liste zusammen in list train
        usdTrain = []
        usdTest = []
        for j in range(len(lst)):
            usdTrain += lst[j]
        # definiert die Anzahl der Testdaten
        anzTestDaten = len(usd) - anzTrainingsdaten
        # zufaellige Zuweisung der von Testdaten
        for n in range(0, anzTestDaten):
            # nr = random.randint(0, len(usdTrain)-1)
            nr = n
            usdTest.append(usdTrain[nr])
            del usdTrain[nr]

        return usdTrain, usdTest


# Auswahl der Berechnungseinheit
def chooseDevice():
    if torch.cuda.is_available():
        return "cuda"
    else:
        return "cpu"


""" 
Zum Laden des postquantisierten Models 
model: Art des Modells
name: name des statedicts (abgespeicherten Modells) was geladen werden soll
data_loader: daten zum kalibrieren des quantisierten Modells
device: cpu/cuda
quantisiert: Wichtig zu setzen für predict des Neuronalen Netzes (siehe cnnNet)
"""
def loadPostQuantModel(model, name, data_loader, device, quantisiert = True ):
    cnn = model.cpu()
    cnn.eval()
    cnn.qconfig = torch.quantization.get_default_qconfig('fbgemm')
    print(cnn.qconfig)
    cnn = torch.quantization.prepare(cnn)  # , inplace=False)
    # Prepare model for quantisation
    # calibrate the prepared model to determine quantization parameters for activations
    # https://pytorch.org/docs/stable/quantization.html
    calibrate(cnn, data_loader, device, quantisiert)
    cnn = cnn.cpu()
    cnn = torch.quantization.convert(cnn)  # inplace=False)
    state_dict = torch.load(os.getcwd() + "\\" + name)
    cnn.load_state_dict(state_dict)
    return cnn

"""
Gibt das gespeicherte unquantisierte NN zurueck
model: Vorgegebene Netzstruktur
name: Name des zu ladendenden statedicts 
"""
def loadFloatModel(model, name):
    state_dict = torch.load(os.getcwd() + "\\" + name)
    model.load_state_dict(state_dict)
    return model
"""
Laed doppelt trainieres Netz
"""
def loadTrainedAgainModel(nameSaveNetwork, foldTraining, train_dataloader, quantisiert, bias, usd, usdTest = None):
    cnn = nn.Sequential(
        torch.quantization.QuantStub(),
        convNet(quantisiert, bias),
        torch.quantization.DeQuantStub()
    )
    if not foldTraining:
        cnn3 = loadPostQuantModel(cnn, nameSaveNetwork, train_dataloader, "cpu", quantisiert)
        darstellenTrueFalse(cnn3, usdTest, len(usdTest), "cpu", quantisiert)
    else:
        cnn3 = loadPostQuantModel(cnn, nameSaveNetwork, train_dataloader, "cpu", quantisiert)
        darstellenTrueFalse(cnn3, usd, len(usd), "cpu", quantisiert)

    return cnn3

"""
Aware Training 
model: Eingabe des Modells
"""
def awareTrainingCNN(cnn, usd, loss_fn, optimiser, device, EPOCHS,  foldDatasetPath = None, foldtraining = False):
    usdTest = []
    # CNN = Config Float32
    cnn.qconfig = torch.quantization.get_default_qat_qconfig('fbgemm')
    # Prepare model for quantisation
    cnn = torch.quantization.prepare_qat(cnn)
    if foldtraining:
        trainFolds(foldDatasetPath, cnn, loss_fn, optimiser, device, EPOCHS)
        EPOCHS = EPOCHS * 10
    else:
        usdTrain, usdTest = splitDataSet(usd, 5000)
        train_dataloader = DataLoader(usdTrain, batch_size=BATCH_SIZE, shuffle=False)
        train(cnn, train_dataloader, loss_fn, optimiser, device, EPOCHS)
    cnn = cnn.cpu()
    cnn.eval()
    cnn = torch.quantization.convert(cnn)
    return cnn, EPOCHS, usdTest  # Quantisiertes Netz

"""
mit AwareTraining trainierte Netz laden
model: CNN Struktur, die geladen werden soll
name: Name des abgespeicherten Modells
"""
def loadAwareTrainedModel(model, name):
    cnn = model.cpu()
    # CNN = Config Float32
    cnn.qconfig = torch.quantization.get_default_qat_qconfig('fbgemm')
    # Prepare model for quantisation
    cnn = torch.quantization.prepare_qat(cnn)
    cnn = torch.quantization.convert(cnn)
    # state_dict = torch.load("cnnnet.pth")
    state_dict = torch.load(os.getcwd() + "\\" + name)
    cnn.load_state_dict(state_dict)
    return cnn

if __name__ == "__main__":
    device = chooseDevice()
    print(f"Using {device}")
    usdTrain = []
    usdTest = []
    saveOption = True # Netz abspeichern if true
    quantisiert = False # Netz mit quantisierten Aufbau if true
    bias = False # if true: CNN mit Bias
    foldTraining = True #, Foldtraining
    trainAgainQuantisized = False# Nur wenn quantizied = False retraining mit postquantisierung
    awareTraining = False # Wenn false und Quantisierung = True wird postquant durchgefuert

    # Test/ Training Datenset
    usd = torch.load(os.getcwd() + "\\Datenset\\datasetQuantisiert44.1kHz.pt")
    foldDataset = "foldsQuantisiert44.1kHz"
    nameSaveNetwork = "quantisiertCNNFoldTraining10x10_OhneBias.pth"
    # Angabe des Modells
    model = convNet(quantisiert, bias)
    # verwende das verfuebare Device
    cnn = model.to(device)

    # initialise loss funtion + optimiser
    loss_fn = nn.CrossEntropyLoss()
    optimiser = torch.optim.Adam(cnn.parameters(),
                                 lr=LEARNING_RATE)
    # Prepare Training
    cnn.train()
    if foldTraining and not awareTraining:
        # Trainiere 10 mal und teste immer auf 1 Fold
        trainFolds(foldDataset, cnn, loss_fn, optimiser, device, EPOCHS)
        EPOCHS = EPOCHS * 10
    elif not foldTraining and not awareTraining:
        # Normales Training mit Test und Trainingsdaten nicht quantisiertes Modell
        usdTrain, usdTest = splitDataSet(usd, 5000)
        train_dataloader = DataLoader(usdTrain, batch_size=BATCH_SIZE, shuffle=False)
        train(cnn, train_dataloader, loss_fn, optimiser, device, EPOCHS)
    elif quantisiert and awareTraining:
        # aware training immer mit quantisierten Netz
        cnn, EPOCHS, usdTest = awareTrainingCNN(cnn, usd, loss_fn, optimiser, device, EPOCHS, foldDataset, foldTraining)

    if quantisiert and not awareTraining:
        # Postquantisierung des trainieren Netzes mit einem QuantStub und DeQuantStub
        cnn = quantisiertesNetzPostQuant(cnn, usd, "cpu", quantisiert)
    elif trainAgainQuantisized and not quantisiert and not awareTraining:
        # neu quantisiertes Netz
        cnn = nn.Sequential(
            torch.quantization.QuantStub(),
            cnn,
            torch.quantization.DeQuantStub()
        )
        # Foldtraining wenn das Netzt 2 mal trainiert werden soll
        if foldTraining:
            trainFolds(foldDataset, cnn, loss_fn, optimiser, device, EPOCHS)
            train_dataloader = DataLoader(usd, batch_size=BATCH_SIZE, shuffle=False)
        else:
            train(cnn, train_dataloader, loss_fn, optimiser, device, EPOCHS)
        cnn = quantisiertesNetzPostQuant(cnn, usd, "cpu", quantisiert)


    # Darstellen des Ergebnisses auf fuer alle Audiofiles
    if foldTraining:
        darstellenTrueFalse(cnn, usd, 8732, "cpu", quantisiert)
    else:
        darstellenTrueFalse(cnn, usdTest, len(usdTest), "cpu", quantisiert)
    # Wenn die Save Option gewaehlt wurde wird das Netz gespeichert
    if saveOption:
        torch.save(cnn.state_dict(), os.getcwd() + "\\" + nameSaveNetwork)
        print("Saved CNN")
        # Laden Testen
        # Wenn Foldtraining verwendet wurde und ein 2tes mal trainiert das floatnetz quantisiert
        if trainAgainQuantisized and not quantisiert:
            cnn = nn.Sequential(
                torch.quantization.QuantStub(),
                convNet(quantisiert, bias),
                torch.quantization.DeQuantStub()
            )
            # lade das postquanttrainierte Netz
            if not foldTraining:
                cnn3 = loadPostQuantModel(cnn, nameSaveNetwork, train_dataloader, "cpu", quantisiert)
                darstellenTrueFalse(cnn3, usdTest, len(usdTest), "cpu", quantisiert)
            else:
                cnn3 = loadPostQuantModel(cnn, nameSaveNetwork, train_dataloader, "cpu", quantisiert)
                darstellenTrueFalse(cnn3, usd, len(usd), "cpu", quantisiert)
        elif awareTraining and quantisiert:
            # Lade das mit Awaretraining trainierte Netz, welches mit foldtraining oder Test- und Trainingsdaten trainiert wurde
            cnn3 = loadAwareTrainedModel(convNet(quantisiert, bias), nameSaveNetwork)
            print("ready aware loaded")
            if foldTraining:
                darstellenTrueFalse(cnn3, usd, len(usd), "cpu", quantisiert)
            else:
                darstellenTrueFalse(cnn3, usdTest, len(usdTest), "cpu", quantisiert)
        elif foldTraining and quantisiert:
            train_dataloader = DataLoader(usd, batch_size=BATCH_SIZE, shuffle=False)
            # Lade das postquantisierte Netz, welches mit Foldtraining trainiert wurde
            cnn3 = loadPostQuantModel(convNet(quantisiert, bias), nameSaveNetwork, train_dataloader, "cpu")
            darstellenTrueFalse(cnn3, usd, len(usd), "cpu", quantisiert)
        elif not foldTraining and quantisiert:
            train_dataloader = DataLoader(usd, batch_size=BATCH_SIZE, shuffle=False)
            # Lade das postquantisierte Netz, welches mit Test- und Trainingsdaten trainiert wurde
            cnn3 = loadPostQuantModel(convNet(quantisiert, bias), nameSaveNetwork, train_dataloader, "cpu")
            darstellenTrueFalse(cnn3, usdTest, len(usdTest), "cpu", quantisiert)
        elif foldTraining and not quantisiert:
            train_dataloader = DataLoader(usd, batch_size=BATCH_SIZE, shuffle=False)
            cnn3 = loadFloatModel(convNet(quantisiert, bias), nameSaveNetwork)
            darstellenTrueFalse(cnn3, usd, len(usd), "cpu", quantisiert)
        elif not foldTraining and not quantisiert:
            train_dataloader = DataLoader(usd, batch_size=BATCH_SIZE, shuffle=False)
            cnn3 = loadFloatModel(convNet(quantisiert, bias), nameSaveNetwork)
            darstellenTrueFalse(cnn3, usdTest, len(usdTest), "cpu", quantisiert)
        print("loaded CNN tested")
    plt.show()
