import torch
import torch.quantization
import numpy as np
import os
import torch.nn.functional as F
import matplotlib.pyplot as plt
from torch.utils.data import DataLoader
from torch import nn
from cnnNet_quantized_or_not import cnnNet
from training import loadPostQuantModel, loadFloatModel,loadTrainedAgainModel, loadAwareTrainedModel, testeWerte, predict, calibrate, EPOCHS

"""
Fuehrt die Postquantisierung auf einem Floatnetz durch 
model: CNN
data_loader: Datenset zur Kalibrierung vor der Postquantisierung 
device: cpu/ cuda 
"""
def PostQuantFloat(model,data_loader, device):
    cnn = model.cpu()
    cnn.eval()
    # fuege notwendige Layer fuer Postquant hinzu
    cnn = nn.Sequential(
        torch.quantization.QuantStub(),
        cnn,
        torch.quantization.DeQuantStub()
    )
    cnn.qconfig = torch.quantization.get_default_qconfig('fbgemm')
    print(cnn.qconfig)
    cnn = torch.quantization.prepare(cnn)  # , inplace=False)
    # Prepare model for quantisation
    calibrate(cnn, data_loader, device, False)
    cnn = cnn.cpu()
    cnn = torch.quantization.convert(cnn)  # inplace=False)
    return cnn

"""
Speichert die Gewichte in int8
path: Speicherpfad
cnn: Network
"""
def storeWeights(path, cnn):
    path_dir = path
    # store
    for name, weight in cnn.state_dict().items():
        print(name)
        try:
            tensor = weight.cpu().detach().int_repr().numpy()
            with open(path_dir + "/" + name + ".npy", 'wb') as f:
                # Anzahl an spalten wird hier definiert
                print(tensor.shape)
                tensor = tensor.reshape(tensor.shape[0], -1)
                np.save(f, tensor)
        except:
            if "conv" not in name and "linear" not in name:
                with open(path_dir + "/" + name + ".npy", 'wb') as f:
                    # Anzahl an spalten wird hier definiert
                    np.save(f, weight)
                continue
    print("-----------------------------")
    # load test
    for name, weight in cnn.state_dict().items():
        try:
            with open(path_dir + "/cnn" + name + ".npy", 'rb') as f:
                a = np.load(f)
                print([name, a])
                print(a.shape)
        except:
            continue


"""
Berechnet wie viele Testsample richtig und falsch nach dem Training praediziert wurden und plottet das Ergebnis 
mit der Genauigkeit des Models
model: CNN
usd: UrbanDatasound extrahierte Melspektrogrammliste
anzTestsample: Anzahl Testaudios
quantisiert: bei true wird die Praediktion des quantisierten Modells verwendet
normiert: bei true ist wird der Input vorher 
mit input = (input - mean(input)) / std(input) normiert
"""
def darstellenTrueFalse(model, usd, anzTestsample, device, quantisiert, epochen=None, normiert=False):
    model.eval()
    richtig, falsch = testeWerte(model, usd, anzTestsample, device, quantisiert, normiert)
    accurancy = 100 * richtig / (richtig + falsch)
    x = np.arange(2)
    plt.figure()
    plt.bar(x, height=[richtig, falsch])
    plt.xticks(x, ['true', 'false'], fontsize=12)
    plt.ylabel('Anzahl Audiofiles', fontsize=12)
    if epochen is None:
        plt.title(f'Spektrogramme: {anzTestsample}\n Accurancy: {accurancy:.2f} %', fontsize=12)
    else:
        plt.title(f'Spektrogramme: {anzTestsample}\n Accurancy: {accurancy:.2f} %, Epochen: {epochen}', fontsize=12)


# path: Pfad zum abgespeicherten Audofile
def histogramAudiosignal(path):
    with open(path + ".npy", 'rb') as f:
        a = np.load(f)
        plt.figure()
        plt.hist(a.flatten())
        plt.ylabel('Anzahl der Werte')
        plt.ylabel('Quantisierte Werte')


"""
Speichert Audiodateien nach dem Quantstub layer des quantisierten Modells aufsteigend nummeriert je Klasse
mit unfold ab
quantModel: Quantisiertes Modell
usd: Datenset 
path: Speicherordner 
"""
def saveAudioQuantisiertAfterQuantstub(quantModel, usd, path):
    anzGesamt = 0
    for i in range(0, len(usd)):
        klasse = usd[i][1]
        if not os.path.exists(path + "/" + str(klasse)):
            os.makedirs(path + "/" + str(klasse))
        anz = len(os.listdir(path + "/" + str(klasse)))
        data = torch.unsqueeze(usd[i][0], 0)
        data = data.to("cpu")
        if isinstance(quantModel, cnnNet):
            data = F.unfold(data, kernel_size=quantModel.conv1[0].kernel_size[0],
                            padding=quantModel.conv1[0].padding[0],
                            stride=quantModel.conv1[0].stride[0])
            data = quantModel.quant(data)
            data = data.cpu().detach().int_repr()
        elif isinstance(quantModel[0], torch.quantization.QuantStub()):
            data = F.unfold(data, kernel_size=quantModel[1].conv1[0].kernel_size[0],
                            padding=quantModel[1].conv1[0].padding[0],
                            stride=quantModel[1].conv1[0].stride[0])
            data = quantModel[0](data)
            data = data.cpu().detach().int_repr()

        outQuantisiert = data.to(dtype=torch.int8)
        tensor = outQuantisiert.cpu().detach().numpy()
        with open(path + "/" + str(klasse) + "/" + str(anz) + ".npy", 'wb') as f:
            np.save(f, tensor)
            anzGesamt = anzGesamt + 1
    print("Insgesamt gespeicherte Audiodateien: " + str(anzGesamt))

"""
Erstellt eine Textdatei mit Gewichten
folder: Wo die Datei abgespeichert werden soll
name: name der Textdatei
cnn: Cnn Modell
"""
def createTxt(folder, name, cnn):
    from pathlib import Path
    myfile = Path(folder + "/" + name + ".txt")
    myfile.touch(exist_ok=True)
    datei = open(myfile, 'a')
    for name, weight in cnn.state_dict().items():
        try:
            datei.write(name + "\r\n ")
            datei.write(str(weight.int_repr()))
        except:
            continue
    datei.close()

""" 
Speichert die Parameter wie Gewichte oder die Eingangsdaten des Netzes nach dem Quantstub Layer
boolAudioData: if true werden die Audiodaten nach dem Quantstub Layer des quantisierten Modells in AudioQuantisiert abgespeichert
boolnetworkParameter: if true werden die Gewichte des Netzes abgespeichert 
modelQuantisiert: Quantisiertes Modell aus dem die Parameter ausgegeben werden sollen oder wo die Audiodaten durch den 
quantstub Layer gehen und gespeichert werden
usd: Datenset
"""
def saveParameter(boolaudioData, boolnetworkParameter, modelQuantisiert, usd):
    if boolaudioData:
        if not os.path.exists(os.getcwd() + "/AudioQuantisiert"):
            os.makedirs(os.getcwd() + "/AudioQuantisiert")
        saveAudioQuantisiertAfterQuantstub(modelQuantisiert, usd, os.getcwd() + "/AudioQuantisiert")
    if boolnetworkParameter:
        if not os.path.exists(os.getcwd() + "/NetzParameter"):
            os.makedirs(os.getcwd() + "/NetzParameter")
        storeWeights(os.getcwd() + "/NetzParameter", modelQuantisiert)

# Ausgabe der Layer des cnn sowie der Datentypen
def networkParameterPrint(cnn):
    for name, weight in cnn.state_dict().items():
        try:
            print([name, weight.dtype, weight.int_repr()])
            print([weight.int_repr().min(), weight.int_repr().max()])
        except:
            continue

# Gibt alle Layer des Netzwerks aus
def networkPrint(cnn):
    for idx, m in enumerate(cnn.named_modules()):
        print(idx, '->', m)

# Erstellt ein Dictionary der Module der Netze
def dictionaryErstellen(cnn):
    data2 = {}
    for name, module in cnn.named_modules():
        try:
            data2[name + "weight"] = module.weight
            data2[name + "bias"] = module.bias
        except:
            continue
    return data2

if __name__ == "__main__":
    # Anpassung der Konfigurationen wie das Netz abgespeichert wurde
    BATCH_SIZE = 32
    quantisiert = True
    foldTraining = True # Trainingsmethode mit Foldtestung
    bias = False # save parameter
    saveParameterNetwork = False # Save Parameter des Netzwerks
    saveSingleAudiofileAfterQuantstub = False # Save Audiofiles after the QuantStub Layer
    trainAgainQuantisized = False # train AgainQuantisiert
    awareTraining = False # if true muss auch quantisiert true sein
    usd = torch.load(
        os.getcwd() + "/Datenset/datasetQuantisiert44.1kHz.pt")  # Path auf dem das geladene Netz getestet/ quantisiert weden soll
    dataloader = DataLoader(usd, batch_size=BATCH_SIZE, shuffle=False)  # Erstellung Datenset fuer Postquant
    cnn = None
    if trainAgainQuantisized and not quantisiert and not awareTraining:
        # Retrained net war am Anfang nicht quantisiert
        cnn = loadTrainedAgainModel("/Netze/doppeltrainingFloatQuant10x10EpochenFoldTraining.pth", foldTraining, dataloader, quantisiert, bias, usd)
    elif quantisiert and not awareTraining:
        cnn = loadPostQuantModel(cnnNet(quantisiert, bias), "/Netze/postquantisiertCNNFoldTraining10x10_OhneBias.pth",
                                 dataloader, "cpu")
    elif not quantisiert and not awareTraining:
        # Wenn das Floatnetz noch quantisiert werden soll
        postQuant = True
        cnn = loadFloatModel(cnnNet(quantisiert, bias), "/Netze/NichtQuantisiertOhneBias10x10EpochenFoldTraining.pth")
        if postQuant:
            cnn = PostQuantFloat(cnn, dataloader, "cpu")
    elif awareTraining and quantisiert:
        cnn = loadAwareTrainedModel(cnnNet(quantisiert, bias),"/Netze/testAwareTrain.pth")

    if cnn == None:
        print("cnn nicht definiert")
    else:
        # Wenn die Gewichte gespeichert werden sollen
        if saveParameterNetwork or saveParameterNetwork:
            saveParameter(saveParameterNetwork, saveParameterNetwork, cnn, usd)

        darstellenTrueFalse(cnn, usd, 8732, "cpu", quantisiert, EPOCHS * 10)
        # Uses one Test Image,
        input, target = usd[0][0], usd[0][1]  # [batch size, num_channels, fr, time]
        input = input.cpu()
        input.unsqueeze_(0)  # fuegt die 4te Dimension hinzu
        predicted = predict(cnn, input, quantisiert)
        print(f"Predicted: '{predicted}', expected: '{target}'")
        plt.show()
